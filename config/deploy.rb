require 'bundler/capistrano'

$:.unshift(File.expand_path('./lib', ENV['rvm_path'])) # Add RVM's lib directory to the load path.
require "rvm/capistrano"
set :rvm_ruby_string, '1.9.2@rails3'
set :rvm_type, :user 

set :application, "imageposter"
set :repository,  "https://bitbucket.org/yujiyokoo/imageposter.git"
set :default_environment, {
  'PATH' => "$PATH:/var/lib/gems/1.9.1/bin/"
}

set :scm_username, 'YOUR_SCM_USERNAME'
set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

set :deploy_to, '/home/deploy/apps/imageposter'
set :deploy_via, 'export'

role :web, "YOUR_WEB_SERVER"                          # Your HTTP server, Apache/etc
role :app, "YOUR_APP_SERVER"                          # This may be the same as your `Web` server
role :db,  "YOUR_DB_SERVER", :primary => true # This is where Rails migrations will run
#role :db,  "your slave db-server here"

default_run_options[:pty] = true

set :user, 'deploy'
set :password, 'YOUR_DEPLOY_PASSWORD'
set :runner, 'www-data'
set :base_url, 'http://YOUR_BASE_URL/'

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end
