class ImagePostsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]
  def index
    cond = nil
    if !current_user.blank? && params[:limit] == 'mine_only'
      cond = ['user_id = ?', current_user.id]
    end
    @image_posts = ImagePost.for_user(current_user).paginate(:page => params[:page], :conditions => cond )
  end
  def new
    @image_post = ImagePost.new
    @image_post.uuid_ref = UUIDTools::UUID.random_create.to_s
  end
  def show
    @image_post = ImagePost.for_user(current_user).find( params[:id] )
    @attached_images = @image_post.posted_images
  end
  def create
    @image_post = ImagePost.new( params[:image_post] )
    @image_post.user_id = current_user.id
    @image_post.posted_images = PostedImage.where( :temp_ref => @image_post.uuid_ref )
    @image_post.posted_images.each { |pi| pi.temp_ref = nil }
    if @image_post.save
      flash[:notice] = 'Your post has been saved successfully'
      redirect_to image_post_path( @image_post )
    else
      flash[:alert] = 'Sorry, could not save your post'
      render :action => 'new'
    end
  end
  def destroy
    @image_post = ImagePost.find(params[:id])
    if current_user.admin? || @image_post.owned_by?( current_user )
      if @image_post.destroy
        flash[:notice] = 'Post deleted successfully'
      end
    else
      flash[:alert] = 'You cannot delete that post'
    end
    redirect_to image_posts_path
  end
end
