require 'spec_helper'

shared_examples_for "approvable" do
  context "when getting approval" do
    it "should be not approved by default" do
      @approvable.approved?.should be_false
    end
    context "when pending" do
      it "should get approved on approve!" do
        @approvable.approve!
        @approvable.approved?.should be_true
      end
      it "should get rejected on reject!" do
        @approvable.reject!
        @approvable.rejected?.should be_true
      end
    end
    context "when approved" do
      before(:each) do
        @approvable.approve!
      end
      it "should raise InvalidTransition approve!" do
        lambda { @approvable.approve! }.should raise_error( AASM::InvalidTransition )
      end
      it "should get rejected on reject!" do
        @approvable.reject!
        @approvable.rejected?.should be_true
      end
    end
    context "when rejected" do
      before(:each) do
        @approvable.reject!
      end
      it "should get approved on approve!" do
        @approvable.approve!
        @approvable.approved?.should be_true
      end
      it "should raise InvalidTransition on reject!" do
        lambda { @approvable.reject! }.should raise_error( AASM::InvalidTransition )
      end
    end
  end
end

