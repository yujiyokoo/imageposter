require 'spec_helper'

describe ImagePostsController do
  before(:each) do
    @image_post = mock_model(ImagePost, :user_id= => nil)
    @scope = mock( ImagePost )
    ImagePost.stub!(:new).and_return(@image_post)
  end
  context "logged in as user" do
    before(:each) do
      @user = FactoryGirl.create( :user )
      sign_in( @user )
    end

    describe "index" do
      it "should find image posts for the user" do
        ImagePost.should_receive(:for_user).with(@user).and_return( @scope )
        @scope.should_receive(:paginate).with(:page => '125', :conditions => nil)
        get :index, :page => '125'
      end
      context "when ?limit=mine_only param given" do
        it "should find image posts for the user without others'" do
          ImagePost.should_receive(:for_user).with(@user).and_return( @scope )
          @scope.should_receive(:paginate).with(:page => '125', :conditions => ['user_id = ?', @user.id])
          get :index, :limit => 'mine_only', :page => '125'
        end
      end
    end

    describe "new" do
      it "should build a new obj with uuid set" do
        @image_post.should_receive( :uuid_ref= )
        get :new
        assigns[:image_post].should ==( @image_post )
      end
    end

    describe "show" do
      it "should find image post for id for the user" do
        @posted = mock( PostedImage )
        ImagePost.should_receive( :for_user ).with(@user).and_return( @scope )
        @scope.should_receive( :find ).with('foo').and_return( @image_post )
        @image_post.should_receive( :posted_images ).and_return( @posted )
        get :show, :id => 'foo'
        assigns[:attached_images].should == @posted
      end
    end

    describe "create" do
      before(:each) do
        ImagePost.stub!(:new).and_return(@image_post)
        @image_post.stub!(:uuid_ref).and_return('uuid')
        @image_post.stub!(:posted_images).and_return( [] )
        @image_post.stub!(:posted_images=)
        @image_post.stub!(:save).and_return(true)
        @posted_image = mock_model(PostedImage)
      end
      it "should make image post based on params" do
        ImagePost.should_receive(:new).with( 'foo' => 'bar' ).and_return(@image_post)
        post :create, :image_post => { :foo => 'bar' }
      end
      it "should set posted images according to uuid ref" do
        @image_post.should_receive( :uuid_ref ).and_return( 'foo' )
        PostedImage.should_receive( :where ).with( :temp_ref => 'foo' ).and_return [@posted_image]
        @image_post.should_receive( :posted_images= ).with( [@posted_image] )
        post :create, :image_post => { }
      end
      it "should set posted images' uuids to nil" do
        @image_post.should_receive(:posted_images).and_return([@posted_image])
        @posted_image.should_receive(:temp_ref=).with(nil)
        post :create, :image_post => { }
      end
      it "should set user id of post" do
        @image_post.should_receive(:user_id=).with(@user.id)
        post :create, :image_post => { }
      end
      describe "when save fails" do
        before(:each) do
          @image_post.stub!(:save).and_return(false)
          post :create
        end
        it "should set notice flash" do
          flash[:notice].should be_blank
          flash[:alert].should_not be_blank
        end
        it "should not redirect" do
          response.should_not be_redirect
          response.should render_template('new')
        end
      end
      describe "when save succeeds" do
        before(:each) do
          @image_post.stub!(:save).and_return(true)
          post :create
        end
        it "should set alert flash" do
          flash[:notice].should_not be_blank
          flash[:alert].should be_blank
        end
        it "should redirect to show view" do
          response.should redirect_to( image_post_path(@image_post) )
        end
      end
    end

    describe "destroy" do
      before(:each) do
        ImagePost.stub!(:find).with('foo').and_return(@image_post)
        @image_post.stub!( :destroy ).and_return( @image_post )
      end
      context "if owned by the user" do
        before(:each) do
          @image_post.stub(:owned_by?).with(@user).and_return( true )
        end
        it "should destroy the image post" do
          @image_post.should_receive(:destroy).and_return( true )
          post :destroy, :id => 'foo'
        end
        it "should redirect to index" do
          post :destroy, :id => 'foo'
          flash[:notice].should_not be_blank
          response.should redirect_to( image_posts_path )
        end
      end
      context "if not owned by the user" do
        before(:each) do
          @image_post.stub(:owned_by?).with(@user).and_return( false )
        end
        it "should not destroy the image post" do
          @image_post.should_not_receive(:destroy)
          post :destroy, :id => 'foo'
        end
        it "should redirect to index" do
          post :destroy, :id => 'foo'
          flash[:alert].should_not be_blank
          response.should redirect_to( image_posts_path )
        end
      end
      context "if the user is admin" do
        before(:each) do
          @image_post.should_not_receive(:owned_by?)
          @user.role = 'admin'
          @user.save!
        end
        it "should destroy the image post" do
          @image_post.should_receive(:destroy).and_return( true )
          post :destroy, :id => 'foo'
        end
        it "should redirect to index" do
          post :destroy, :id => 'foo'
          flash[:notice].should_not be_blank
          response.should redirect_to( image_posts_path )
        end
      end
    end
  end
  context "not logged in as user" do
    describe "index" do
      it "should show index" do
        ImagePost.should_receive(:for_user).with(@user).and_return( @scope )
        @scope.should_receive(:paginate).with(:page => '125', :conditions => nil)
        get :index, :page => '125'
      end
    end
    describe "new" do
      it "should rederect to login screen" do
        get :new
        response.should redirect_to( new_user_session_path )
      end
    end
    describe "show" do
      it "should rederect to login screen" do
        @posted = mock( PostedImage )
        ImagePost.should_receive( :for_user ).with(nil).and_return( @scope )
        @scope.should_receive( :find ).with('foo').and_return( @image_post )
        @image_post.should_receive( :posted_images ).and_return( @posted )
        get :show, :id => 'foo'
        assigns[:attached_images].should == @posted
      end
    end
    describe "create" do
      it "should rederect to login screen" do
        post :create
        response.should redirect_to( new_user_session_path )
      end
    end
    describe "destroy" do
      it "should rederect to login screen" do
        post :destroy, :id => 'foo'
        response.should redirect_to( new_user_session_path )
      end
    end
  end
end

