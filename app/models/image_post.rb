class ImagePost < ActiveRecord::Base
  include AASM
  aasm :column => :state do
    state :pending, :initial => true
    state :approved
    state :rejected

    event :approve, :after => :approve_images do
      transitions :to => :approved, :from => [:pending, :rejected]
    end
    event :reject, :after => :reject_images do
      transitions :to => :rejected, :from => [:pending, :approved]
    end
  end

  has_many :posted_images, :dependent => :destroy
  belongs_to :user

  accepts_nested_attributes_for :posted_images,
    :reject_if => lambda { |image| image['image'].nil? }

  validates_presence_of :title, :description

  attr_accessor :uuid_ref

  scope :approved, :conditions => { :state => 'approved' }
  scope :pending, :conditions => { :state => 'pending' }
  scope :for_user, lambda { |user|
    if( user.try(:admin?) )
      { :conditions => nil }
    elsif( user )
      { :conditions => [ "state = 'approved' or user_id = ?", user.id ] }
    else
      approved
    end
  }

  self.per_page = 10

  # this won't produce shorter than 8
  def simple_ellipsize(str, limit)
    if str.length <= 8
      return str
    elsif limit <= 8
      str[0..4] + "..."
    elsif str.length > limit
      str[0..limit-4] + "..."
    else
      str
    end
  end

  def ellipsized_title( limit = 40 )
    simple_ellipsize( title, limit )
  end

  def ellipsized_description( limit = 80 )
    simple_ellipsize( description, limit )
  end

  def owned_by?( user )
    !user.blank? && user.id == self.user_id
  end

  # note posted images' states are not utilised at the moment!
  def approve_images
    posted_images.each { |pi|
      pi.approve!
    }
  end

  # note posted images' states are not utilised at the moment!
  def reject_images
    posted_images.each { |pi|
      pi.reject!
    }
  end
end
