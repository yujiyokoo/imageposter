class RegistrationsController < Devise::RegistrationsController
  def create
    super
    if( User.count == 1 )
      @only_user = User.last
      @only_user.role = 'admin'
      @only_user.save!
      flash[:notice] += ' As you are the first user, you are now an admin.'
    end
  end
end
