module ImagePostsHelper
  def approve_link( post )
    button_to 'Approve', approvals_path( :image_post_id => post, :type => 'approve' ), :method => 'post'
  end

  def reject_link( post )
    button_to 'Reject', approvals_path( :image_post_id => post, :type => 'reject' ), :method => 'post'
  end

  def approval_links( post )
    case( post.state )
    when 'approved'
      rval = reject_link( post )
    when 'rejected'
      rval = approve_link( post )
    when 'pending'
      rval = approve_link( post ) + ' ' + reject_link( post ) 
    end
  end

  def switch_mine_only_link
    if( params[:limit] == 'mine_only' )
      link_to "List everyone's posts", image_posts_path
    else
      link_to 'Only list my own posts', image_posts_path( :limit => :mine_only )
    end
  end
end
