class CreatePostedImages < ActiveRecord::Migration
  def change
    create_table :posted_images do |t|
      t.integer 'image_post_id'
      t.string 'name'
      t.text 'description'

      t.timestamps
    end
  end
end
