require 'spec_helper'

describe ApprovalsController do
  before(:each) do
    @user = FactoryGirl.create( :user )
    sign_in( @user )
    @image_post = mock_model( ImagePost )
    ImagePost.stub!( :find ).with( 'foo' ).and_return @image_post
    @image_post.stub( :approve! ).and_return true
  end

  context "logged in as admin" do
    before(:each) do
      @user = FactoryGirl.create( :admin )
      sign_in( @user )
    end
    context "create" do
      context "successful" do
        it "should approve post" do
          @image_post.should_receive( :approve! ).and_return true
          post :create, { image_post_id: 'foo', type: 'approve' }
          flash[:notice].should_not be_blank
        end
        it "should reject post" do
          @image_post.should_receive( :reject! ).and_return true
          post :create, { image_post_id: 'foo', type: 'reject' }
          flash[:notice].should_not be_blank
        end
      end
      context "unsuccessful" do
        it "should set alert flash (approve)" do
          @image_post.should_receive( :approve! ).and_return false
          post :create, { image_post_id: 'foo', type: 'approve' }
          flash[:alert].should_not be_blank
        end
        it "should set alert flash (reject)" do
          @image_post.should_receive( :reject! ).and_return false
          post :create, { image_post_id: 'foo', type: 'reject' }
          flash[:alert].should_not be_blank
        end
      end
      it "should redirect to image posts index" do
        post :create, { image_post_id: 'foo', type: 'approve' }
        response.should redirect_to( image_posts_path )
      end
    end
  end

  ["logged in as user", "not logged in"].each { |desc|
    context desc do
      it "should not approve" do
        @image_post.should_not_receive( :approve! )
        @image_post.should_not_receive( :reject! )
        post :create, { image_post_id: 'foo', type: 'approve' }
      end
      it "should redirect to login page" do
        post :create, { image_post_id: 'foo', type: 'approve' }
        response.should redirect_to( image_posts_path )
      end
      it "should set alert flash" do
        post :create, { image_post_id: 'foo', type: 'approve' }
        flash[:alert].should_not be_blank
      end
    end
  }

end
