namespace :imageposter do
  task :clear_old_images => :environment do
    limit = 3.days.ago # surely no one needs 3 days to complete creating posts.
    # XXX: use delete_all for performance? but we have paperclip..
    old_images = PostedImage.find( :all, :conditions => [ 'image_post_id is null and updated_at < ?', limit ] )
    puts "Deleting #{old_images.size} image(s)..."
    old_images.each( &:destroy )
  end
end
