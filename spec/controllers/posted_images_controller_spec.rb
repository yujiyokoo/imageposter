require 'spec_helper'

describe PostedImagesController do
  before(:each) do
    @posted_image = mock_model( PostedImage )
    PostedImage.stub!(:find).and_return(@posted_image)
    PostedImage.stub!(:new).and_return(@posted_image)
    @user = FactoryGirl.create( :user )
    sign_in( @user )
    @scope = mock( PostedImage )
  end
  describe "show" do
    it "should find posted image for user with id" do
      PostedImage.should_receive(:owned_by).with( @user ).and_return @scope
      @scope.should_receive(:find).with( 'foo' )
      get :show, :id => 'foo'
    end
  end
  describe "new" do
    it "should build posted image with temp ref" do
      PostedImage.should_receive(:new).with( :temp_ref => 'foo' )
      get :new, :image_post_id => 'foo'
    end
  end
  describe "create" do
    before(:each) do
      @posted_image.stub!(:save).and_return(true)
      @posted_image.stub!(:user=)
    end
    it "should build posted image with params" do
      PostedImage.should_receive(:new).with( 'foo' => 'bar' )
      post :create, :posted_image => { :foo => 'bar' }
    end
    it "should set user to current user" do
      @posted_image.should_receive(:user=).with(@user)
      post :create, :posted_image => { :foo => 'bar' }
    end
    describe "when save succeeds" do
      before(:each) do
        @posted_image.should_receive(:save).and_return(true)
      end
      it "should redirect to show" do
        post :create, :posted_image => { :foo => 'bar' }
        response.should redirect_to( posted_image_path(@posted_image) )
      end
    end
    describe "when save fails" do
      before(:each) do
        @posted_image.should_receive(:save).and_return(false)
      end
      it "should render new with no layout" do
        post :create, :posted_image => { :foo => 'bar' }
        response.should render_template('new')
      end
    end
  end
  describe "update" do
    before(:each) do
      @posted_image.stub(:update_attributes).and_return(true)
      request.stub(:xhr?).and_return( true )
    end
    it "should find posted image for id" do
      PostedImage.should_receive(:find).with( 'foo' )
      post :update, :id => 'foo', :format => 'html'
    end
    it "should update according to attribute" do
      @posted_image.should_receive(:update_attributes).with( 'foo' => 'bar' )
      post :update, :id => 'foo', :posted_image => { :foo => 'bar' }, :format => 'html'
    end
    describe "when update attributes succeeds" do
      before(:each) do
        @posted_image.stub!(:update_attributes).and_return(true)
      end
      it "should render text ok" do
        post :update, :id => 'foo', :posted_image => { :foo => 'bar' }, :format => 'html'
        response.body.should == 'OK'
      end
    end
    describe "when update attributes fails" do
      before(:each) do
        @posted_image.stub!(:update_attributes).and_return(false)
      end
      it "should render text fail" do
        post :update, :id => 'foo', :posted_image => { :foo => 'bar' }, :format => 'html'
        response.body.should == 'FAIL'
      end
    end
  end
end
