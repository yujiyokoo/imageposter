require 'paperclip_processors/watermark'
class PostedImage < ActiveRecord::Base
  # note these are set but not utilised at the moment!
  include AASM
  aasm :column => :state do
    state :pending, :initial => true
    state :approved
    state :rejected

    event :approve do
      transitions :to => :approved, :from => [:pending, :rejected]
    end
    event :reject do
      transitions :to => :rejected, :from => [:pending, :approved]
    end
  end
  
  belongs_to :image_post
  belongs_to :user
  has_attached_file :image,
    :processors => [:watermark],
    :styles => {
      :thumb => "64x64",
      :large => {
        :geometry => '512x512',
        :watermark_path => "#{Rails.root}/public/images/watermark.png",
        :position => 'Center'
      }
    }
  validates_attachment_presence :image
  validates_attachment_size :image, :less_than => 5.megabytes
  validates_attachment_content_type :image, :content_type => ['image/jpeg', 'image/png', 'image/bmp', 'image/gif', 'image/tiff']

  scope :approved, :conditions => { :state => 'approved' }
  scope :pending, :conditions => { :state => 'pending' }

  scope :owned_by, lambda { |user|
    { :conditions => [ "user_id = ?", user.id ] }
  }
end
