require 'spec_helper'

describe User do
  it { should have_many :posted_images }

  before(:each) do
    @user = User.new
  end
  context "when role is admin" do
    before(:each) do
      @user.role = 'admin'
    end
    it "should return true to admin? query" do
      @user.admin?.should be_true
    end
  end
  context "when role is not admin" do
    before(:each) do
      @user.role = 'user'
    end
    it "should return false to admin? query" do
      @user.admin?.should be_false
    end
  end
end

