class PostedImagesController < ApplicationController
  before_filter :authenticate_user!
  def show
    @posted_image = PostedImage.owned_by(current_user).find(params[:id])
    render :layout => false
  end

  def new
    # XXX: this is done by /image_post/<uuid>/posted_image/new.
    # So the :image_post_id is not really an existing image_post's id.
    # Is there a better way of doing this?
    @posted_image = PostedImage.new( :temp_ref => params[:image_post_id] )
    render :layout => false
  end

  def create
    # TODO: validate post image so that it either has image post or new ref set.
    @posted_image = PostedImage.new(params[:posted_image])
    @posted_image.user = current_user
    if @posted_image.save
      redirect_to( posted_image_path(@posted_image) )
    else
      render :action => 'new', :layout => false
    end
  end

  def update
    @posted_image = PostedImage.find(params[:id])
    # TODO: better results esp. for error
    if @posted_image.update_attributes( params[:posted_image] )
      render :text => 'OK' if request.xhr?
    else
      render :text => 'FAIL' if request.xhr?
    end
  end
end
