class ApprovalsController < ApplicationController
  before_filter :authenticate_user!
  def create
    if current_user.admin?
      @image_post = ImagePost.find( params[:image_post_id] )
      if params[:type] == 'approve'
        if @image_post.approve!
          flash[:notice] = 'Post approved.'
        else
          flash[:alert] = 'Failed to approve post.'
        end
      elsif params[:type] == 'reject'
        if @image_post.reject!
          flash[:notice] = 'Post rejected.'
        else
          flash[:alert] = 'Failed to reject post.'
        end
      end
    else
      flash[:alert] = 'You must be admin to approve posts.'
    end
    redirect_to( image_posts_path )
  end
end
