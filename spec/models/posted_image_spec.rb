require 'spec_helper'
require 'models/shared_examples'

describe PostedImage do

  # TODO: named scope?

  before(:each) do
    @approvable = PostedImage.new( :image_file_name => File.join( Rails.root, 'spec', 'img.png' ) )
    @approvable.save!
  end

  it { should belong_to :image_post }

  it { should belong_to :user }

  it_behaves_like "approvable"

end
