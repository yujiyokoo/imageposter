# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

@submit_file_form = (elem) ->
  $(elem).parent("form").submit()

@add_file_field = (uuid) ->
  new_frame = $( "<iframe class=\"image_upload_frame\" src=\"/image_posts/#{ uuid }/posted_images/new\">" )
  new_frame.insertBefore($(window.parent.document).find('input[type=submit]'))
  new_frame.css('display', 'block')

@update_name = (destination, elem, imageid) ->
  update_name_or_desc(destination, elem, imageid, 'name')

@update_desc = (destination, elem, imageid) ->
  update_name_or_desc(destination, elem, imageid, 'desc')

@update_name_or_desc = (destination, elem, imageid, target) ->
  $.ajax(
    url: destination
    data: elem.serialize()
    type: 'PUT'
    beforeSend: () -> $("#posted_image_#{imageid}_#{target}_spinner").show()
    complete: () -> $("#posted_image_#{imageid}_#{target}_spinner").hide()
  )

$(document).ready(
  ()->
    $('.image_upload_file_field').change(
      (event)->
        submit_file_form(this)
        add_file_field($('#posted_image_temp_ref').val())
        $.each(
          $(this).parent().children('input'),
          (idx, sibling)-> $(sibling).attr('disabled', 'disabled')
        )
    )
)

