class AddUserIdToPostedImages < ActiveRecord::Migration
  def change
    add_column :posted_images, :user_id, :integer
  end
end
