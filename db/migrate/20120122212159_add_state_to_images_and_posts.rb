class AddStateToImagesAndPosts < ActiveRecord::Migration
  def change
    add_column :image_posts, :state, :string, :default => 'pending'
    add_column :posted_images, :state, :string, :default => 'pending'
  end
end
