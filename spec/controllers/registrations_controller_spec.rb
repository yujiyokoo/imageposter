require 'spec_helper'

describe RegistrationsController do
  before(:each) do
    @new_user = mock( User )
    User.stub!(:last).and_return( @new_user )
    @new_user.stub( :role= )
    @new_user.stub( :save! ).and_return( true )

    # devise related stuff...
    @new_user.stub( :save ).and_return( true )
    @new_user.stub( :active_for_authentication? ).and_return( true )
    RSpec::Mocks::Mock.stub( :serialize_into_session ).and_return( [] )
    @new_user.stub( :is_a? ).and_return( true )
    @new_user.stub( :extractable_options? ).and_return( false )
    request.env["devise.mapping"] = Devise.mappings[:user]
  end
  context "when there is no other user" do
    before(:each) do
      User.stub!(:count).and_return(1)
    end
    it "should make the registered user an admin" do
      @new_user.should_receive( :role= ).with( 'admin' )
      post :create, :user => { :email => Faker::Internet.email, :password => 'password', :password_confirmation => 'password' }
    end
    it "should tell the registered user they are an admin" do
      post :create, :user => { :email => Faker::Internet.email, :password => 'password', :password_confirmation => 'password' }
      flash[:notice].should match( /you.*are.*admin/i )
    end
  end
  context "when there is another user" do
    before(:each) do
      User.stub!(:count).and_return(2)
    end
    it "should not make the registered user an admin" do
      @new_user.should_not_receive( :role= ).with( 'admin' )
      post :create, :user => { :email => Faker::Internet.email, :password => 'password', :password_confirmation => 'password' }
    end
    it "should not tell the registered user they are an admin" do
      post :create, :user => { :email => Faker::Internet.email, :password => 'password', :password_confirmation => 'password' }
      flash[:notice].should_not match( /you.*are.*admin/i )
    end
  end
end
