require 'spec_helper'
require 'models/shared_examples'

describe ImagePost do
  before(:each) do
    @image_post = ImagePost.new(
      :title => 'imagepost',
      :description => 'imagepost description'
    )
    @image_post.save!
    @approvable = @image_post
  end

  it { should validate_presence_of :title }
  it { should validate_presence_of :description }
  it { should have_many :posted_images }

  it_behaves_like "approvable"

  context "when it has posted images" do
    before(:each) do
      @image_post.posted_images << PostedImage.new(
        :image_file_name => File.join( Rails.root, 'spec', 'img.png' )
      )
      @image_post.posted_images << PostedImage.new(
        :image_file_name => File.join( Rails.root, 'spec', 'img.png' )
      )
      @image_post.save!
    end
    it "should approve images when approved" do
      @image_post.approve!
      @image_post.posted_images[0].approved?.should be_true
      @image_post.posted_images[1].approved?.should be_true
    end
    it "should reject images when rejected" do
      @image_post.reject!
      @image_post.posted_images[0].rejected?.should be_true
      @image_post.posted_images[1].rejected?.should be_true
    end
    context "and in pending state" do
      context "and image gets approved" do
        before(:each) do
          @image_post.posted_images[0].approve!
        end
        it "should stay pending" do
          @image_post.pending?.should be_true
        end
      end
      context "and image gets rejected" do
        before(:each) do
          @image_post.posted_images[0].reject!
        end
        it "should stay pending" do
          @image_post.pending?.should be_true
        end
      end
    end
    context "and in approved state" do
      before(:each) do
        @image_post.approve!
      end
      context "and image gets rejected" do
        before(:each) do
          @image_post.posted_images[0].reject!
        end
        it "should stay approved" do
          @image_post.approved?.should be_true
        end
      end
    end
    context "and in rejected state" do
      before(:each) do
        @image_post.reject!
      end
      context "and image gets approved" do
        before(:each) do
          @image_post.posted_images[0].approve!
        end
        it "should stay rejected" do
          @image_post.rejected?.should be_true
        end
      end
    end
  end

  it "should have instance variable 'uuid_ref'" do
    lambda { @image_post.uuid_ref = 'foo' }.should_not raise_error
    lambda { @image_post.non_existent = 'foo' }.should raise_error
  end

  it "should be an instance of imagepost" do
    @image_post.should be_an_instance_of(ImagePost)
  end

  it "should have per_page" do
    ImagePost.per_page.should eql(10)
  end

  it "should ellipsize long string to a limit" do
    @image_post.simple_ellipsize('a' * 50, 15).size.should eql(15)
  end

  it "should not ellipsize short string" do
    @image_post.simple_ellipsize('a' * 8, 5).should eql('aaaaaaaa')
  end

  describe "title longer than 40" do
    before(:each) do
      @image_post.title = 'a' * 50
    end

    it "should ellipsize title to 40" do
      @image_post.ellipsized_title.should eql('a' * 37 + '...')
    end

    it "should ellipsize title to length 8 if limit too small" do
      @image_post.ellipsized_title(3).should eql('aaaaa...')
    end
  end

  describe "title shorter than 40" do
    before(:each) do
      @image_post.title = 'a' * 20
    end

    it "should return all for ellipsize title" do
      @image_post.ellipsized_title.should eql(@image_post.title)
    end
  end

  describe "description longer than 80" do
    before(:each) do
      @image_post.description = 'a' * 90
    end

    it "should ellipsize description to 80" do
      @image_post.ellipsized_description.should eql('a' * 77 + '...')
    end

    it "should ellipsize description to a given number" do
      @image_post.ellipsized_description(3).should eql('aaaaa...')
    end
  end
end
