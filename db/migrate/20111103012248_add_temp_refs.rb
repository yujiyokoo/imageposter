class AddTempRefs < ActiveRecord::Migration
  def up
    add_column :posted_images, :temp_ref, :string
  end

  def down
    remove_column :posted_images, :temp_ref
  end
end
